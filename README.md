
##About

This is a simple project comprising of index.html file and app.js file in the root directory.
To run the application, double click on index.html.

The objective will be to learn JS syntax, and learn how to find programming solutions with google. These are the most important skills you need to develop at this stage.

##Task

In the app.js file, you will notice an empty method called 'addName'.

I want you to fill this in to create a simple function that prints your name on the webpage. I have already saved the DOM element to the variable 'result'.

So for your first task:

* Please create 2 variables. 'firstName' and 'lastName' and set their values appropriately.
* Now concatonate those variables into a single string, and add them as textContent to the result variable.

NB. think about the space inbetween words and how you will handle that.

Some links you may find useful. If you get stuck, google is your friend. Only ask me for help as a very last resort.

https://www.w3schools.com/jsref/prop_node_textcontent.asp  
https://www.w3schools.com/js/js_operators.asp  